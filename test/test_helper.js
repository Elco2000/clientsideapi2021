const mongoose = require('mongoose');
const connect = require('../connect');
require('dotenv').config()

before(done => {
    mongoose.connect(`${process.env.MONGO_URL}/${process.env.MONGO_TEST_DB}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useFindAndModify: false
    });
    mongoose.connection
        .once('open', () => done())
        .on('error', err => {
            console.warn('Warning', err)
        });
    // connect.neo(process.env.NEO4J_TEST_DB);
});

beforeEach(done => {
    const {artists} = mongoose.connection.collections;
    const {bands} = mongoose.connection.collections;
    const {songs} = mongoose.connection.collections;

    Promise.all([artists.drop(), bands.drop(), songs.drop()])
        .then(() => done())
        .catch(() => done());
});
