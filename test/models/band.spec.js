const chai = require('chai')
const expect = chai.expect

const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Band = require('../../src/models/band');

describe('band model', function () {

    it('should create a valid band', function (done) {
        const testBand = new Band({
            name: 'John Doe Band',
            stillActive: true,
            artists: []
        })

        testBand.validate(function(err) {
            expect(err).to.not.exist;
            expect(testBand).to.exist;
            done();
        })
    })

    it('should reject if a value is missing', function (done) {
        const testBand = new Band({
            name: 'John Doe Band',
            // stillActive is missing..
            artists: []
        })

        testBand.validate(function (err) {
            expect(err.errors.stillActive).to.exist;
            done();
        })
    })

    it('should reject if a value is too long', function (done) {
        const testBand = new Band({
            name: 'John Doe Band, John Doe Band, John Doe Band',
            stillActive: true,
            artists: []
        })

        testBand.validate(function (err) {
            expect(err.errors.name).to.exist;
            done();
        })
    })

    it('should reject if a value is too short', function (done) {
        const testBand = new Band({
            name: 'J',
            stillActive: true,
            artists: []
        })

        testBand.validate(function (err) {
            expect(err.errors.name).to.exist
            done();
        })
    })

    it('should reject if a value has a wrong type', function (done) {
        const testBand = new Band({
            name: "John Doe Band",
            stillActive: 5,
            artists: []
        })

        testBand.validate(function (err) {
            expect(err.errors.stillActive).to.exist;
            done();
        })
    })
})
