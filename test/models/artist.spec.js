const chai = require('chai')
const expect = chai.expect

const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Artist = require('../../src/models/artist');

describe('artist model', function () {

    it('should create a valid artist', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        testArtist.validate(function(err) {
            expect(err).to.not.exist;
            expect(testArtist).to.exist;
            done();
        })
    })

    it('should reject if a value is missing', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            // Nickname is missing..
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        testArtist.validate(function (err) {
            expect(err.errors.nickname).to.exist;
            done();
        })
    })

    it('should reject if a value is too long', function (done) {
        const testArtist = new Artist({
            firstname: 'Testname Testname Testname Testname',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        testArtist.validate(function (err) {
            expect(err.errors.firstname).to.exist;
            done();
        })
    })

    it('should reject if a value is too short', function (done) {
        const testArtist = new Artist({
            firstname: 'A',
            lastname: 'B',
            nickname: 'C',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        testArtist.validate(function (err) {
            expect(err.errors.firstname).to.exist;
            expect(err.errors.lastname).to.exist;
            expect(err.errors.nickname).to.exist;
            done();
        })
    })

    it('should reject if a value has a wrong type', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: 'test true'
        })

        testArtist.validate(function (err) {
            expect(err.errors.stillActive).to.exist;
            done();
        })
    })
})
