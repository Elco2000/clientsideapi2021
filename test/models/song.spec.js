const chai = require('chai')
const expect = chai.expect

const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Artist = require('../../src/models/artist');
const Song = require("../../src/models/song");

describe('song model', function () {

    it('should create a valid song', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        const testSong = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.nl',
            artistOfSong: testArtist,
            comments: []
        })

        testSong.validate(function(err) {
            expect(err).to.not.exist;
            expect(testArtist).to.exist;
            done();
        })
    })

    it('should reject if a value is missing', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        const testSong = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            // Youtubelink is missing..
            spotifylink: 'www.test.nl',
            artistOfSong: testArtist,
            comments: []
        })

        testSong.validate(function (err) {
            expect(err.errors.youtubelink).to.exist;
            done();
        })
    })

    it('should reject if a value is too long', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        const testSong = new Song({
            title: 'Born in the USA',
            length: '2:02:02:01',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.nl',
            artistOfSong: testArtist,
            comments: []
        })

        testSong.validate(function (err) {
            expect(err.errors.length).to.exist;
            done();
        })
    })

    it('should reject if a value is too short', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        const testSong = new Song({
            title: 'H',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'H.nu.nl',
            spotifylink: 'H.nu.nl',
            artistOfSong: testArtist,
            comments: []
        })

        testSong.validate(function (err) {
            expect(err.errors.title).to.exist;
            expect(err.errors.youtubelink).to.exist;
            expect(err.errors.spotifylink).to.exist;
            done();
        })
    })

    it('should reject if a value has a wrong type', function (done) {
        const testArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Johny',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        })

        const testSong = new Song({
            title: 'Born in the USA',
            length: 12,
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.nl',
            artistOfSong: testArtist,
            comments: []
        })

        testSong.validate(function (err) {
            expect(err.errors.length).to.exist;
            done();
        })
    })
})
