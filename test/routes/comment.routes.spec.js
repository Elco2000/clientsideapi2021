const request = require("supertest");
const app = require("../../app");
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const assert = require("assert");
const Artist = require("../../src/models/artist");
const Comment = require("../../src/models/comment.schema");
const Song = mongoose.model('song');
const {Schema} = mongoose;

const commentSchema = new Schema({
    Comment
})
const CommentSong = mongoose.model('CommentSong', commentSchema);

describe('Comment routing', () => {
    let songForComments

    beforeEach((done) => {
        const artistSong = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        songForComments = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.com',
            artistOfSong: artistSong,
            comments: []
        });

        Promise.all([songForComments.save()])
            .then(() => done());
    })

    it('POST /api/songs/:id/comment', (done) => {
        const comment = new CommentSong({
            title: 'Test',
            text: 'Test comment',
            dateOfCreate: new Date(),
            user: '1'
        })

        jwt.sign({id: 1}, "secret", {expiresIn: '2h'}, (err, token) => {
            comment.save()
                .then(() => {
                    request(app)
                        .post(`/api/songs/${songForComments._id}/comment`)
                        .set("authorization", "Bearer " + token)
                        .send({comment})
                        .end(() => {
                            Song.findOne({title: 'Born in the USA'})
                                .then(song => {
                                    assert(song.comments.length === 1);
                                    assert(song.title === 'Born in the USA');
                                    done();
                                })
                        })
                })
        })
    })
})
