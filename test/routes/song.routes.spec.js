const assert = require('assert');
const request = require('supertest');
const app = require('../../app');
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const {DateTime} = require("neo4j-driver");
const Artist = require("../../src/models/artist");
const Band = require("../../src/models/band");
const Song = mongoose.model('song');

describe('Song routing', () => {

    it('GET /api/songs', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        const song = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.com',
            artistOfSong: artist,
            comments: []
        });

        song.save()
            .then(() => {
                request(app)
                    .get('/api/songs')
                    .end(() => {
                        Song.find()
                            .then((songs) => {
                                assert(songs.length === 1);
                                assert(songs[0].title === 'Born in the USA');
                                done();
                            })
                    })
            })
    });

    it('GET /api/songs/:id', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        const song = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.com',
            artistOfSong: artist,
            comments: []
        });

        song.save()
            .then(() => {
                request(app)
                    .get(`/api/songs/${song._id}`)
                    .end(() => {
                        Song.findOne({_id: song._id})
                            .then((song) => {
                                assert(song.title === 'Born in the USA');
                                done();
                            })
                    })
            })
    })

    it('GET /api/songs/artists/:id', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        const song = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.com',
            artistOfSong: artist,
            comments: []
        });

        song.save()
            .then(() => {
                request(app)
                    .get(`/api/songs/artists/${song.artistOfSong._id}`)
                    .end(() => {
                        Song.find()
                            .then((songs) => {
                                assert(songs.length === 1);
                                assert(songs[0].title === 'Born in the USA');
                                done();
                            })
                    })
            })
    })

    it('GET /api/songs/bands/:id', (done) => {
        const band = new Band({
            name: 'John Doe Band',
            stillActive: true,
            artists: []
        })

        const song = new Song({
            title: 'Born in the USA',
            length: '2:02',
            releasedate: new Date(),
            youtubelink: 'www.test.nl',
            spotifylink: 'www.test.com',
            bandOfSong: band,
            comments: []
        });

        song.save()
            .then(() => {
                request(app)
                    .get(`/api/songs/artists/${song.bandOfSong._id}`)
                    .end(() => {
                        Song.find()
                            .then((songs) => {
                                assert(songs.length === 1);
                                assert(songs[0].title === 'Born in the USA');
                                done();
                            })
                    })
            })
    })

    it('POST /api/songs', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            Song.countDocuments().then(count => {
                request(app)
                    .post('/api/songs')
                    .set("authorization", "Bearer " + token)
                    .send({
                        title: 'Born in the USA',
                        length: '2:02',
                        releasedate: new Date(),
                        youtubelink: 'www.test.nl',
                        spotifylink: 'www.test.com',
                        artistOfSong: artist,
                        comments: []
                    })
                    .end(() => {
                        Song.countDocuments().then(newCount => {
                            assert(count + 1 === newCount);
                            done();
                        })
                    })
            })
        })
    })
})
