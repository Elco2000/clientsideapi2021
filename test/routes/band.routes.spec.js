const request = require("supertest");
const app = require("../../app");
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const assert = require("assert");
const Artist = mongoose.model('artist');
const Band = mongoose.model('band');


describe('Band routing', () => {
    let bandArtist

    beforeEach((done) => {
        bandArtist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        Promise.all([bandArtist.save()])
            .then(() => done());
    })


    it('GET /api/bands', (done) => {
        const band = new Band({
            name: "John Doe Band",
            stillActive: true,
            artists: []
        });

        band.save()
            .then(() => {
                request(app)
                    .get('/api/bands')
                    .end(() => {
                        Band.find()
                            .then((bands) => {
                                assert(bands.length === 1);
                                assert(bands[0].name === 'John Doe Band');
                                done();
                            })
                    })
            })
    })

    it('GET /api/bands/:id', (done) => {
        const band = new Band({
            name: "John Doe Band",
            stillActive: true,
            artists: []
        });

        band.save()
            .then(() => {
                request(app)
                    .get(`/api/bands/${band._id}`)
                    .end(() => {
                        Band.findOne({_id: band._id})
                            .then((band) => {
                                assert(band.name === 'John Doe Band');
                                done();
                            })
                    })
            })
    })

    it('POST /api/bands', (done) => {
        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            Band.countDocuments().then(count => {
                request(app)
                    .post('/api/bands')
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "John Doe Band",
                        stillActive: true,
                        artists: []
                    })
                    .end(() => {
                        Band.countDocuments().then(newCount => {
                            assert(count + 1 === newCount);
                            done();
                        })
                    })
            })
        })
    })

    it('PUT /api/bands/:id', (done) => {
        const band = new Band({
            name: "John Doe Band",
            stillActive: true,
            artists: []
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            band.save()
                .then(() => {
                    request(app)
                        .put(`/api/bands/${band._id}`)
                        .set("authorization", "Bearer " + token)
                        .send({name: 'Piet Jan Band'})
                        .end(() => {
                            Band.findOne({name: 'Piet Jan Band'})
                                .then(band => {
                                    assert(band.name === 'Piet Jan Band');
                                    done();
                                })
                        })
                })
        })
    })

    it('DELETE /api/bands/:id', (done) => {
        const band = new Band({
            name: "John Doe Band",
            stillActive: true,
            artists: []
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            band.save()
                .then(() => {
                    request(app)
                        .delete(`/api/bands/${band._id}`)
                        .set("authorization", "Bearer " + token)
                        .end(() => {
                            Band.findOne({_id: band._id})
                                .then((band) => {
                                    assert(band === null);
                                    done();
                                })
                        })
                })
        })
    })

    // Band with Artists Func.
    it('POST /api/bands/add/:bId/:aId', (done) => {
        const band = new Band({
            name: "John Doe Band",
            stillActive: true,
            artists: []
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            band.save()
                .then(() => {
                    request(app)
                        .post(`/api/bands/add/${band._id}/${bandArtist._id}`)
                        .set("authorization", "Bearer " + token)
                        .end(() => {
                            Band.findOne({name: 'John Doe Band'})
                                .then(band => {
                                    assert(band.artists.length === 1);
                                    assert(band.name === 'John Doe Band');
                                    done();
                                })
                        })
                })
        })
    })

    it('DELETE /api/bands/remove/:bId/:aId', (done) => {
        const band = new Band({
            name: "John Doe Band",
            stillActive: true,
            artists: [bandArtist._id]
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            band.save()
                .then(() => {
                    request(app)
                        .delete(`/api/bands/remove/${band._id}/${bandArtist._id}`)
                        .set("authorization", "Bearer " + token)
                        .end(() => {
                            Band.findOne({name: 'John Doe Band'})
                                .then(band => {
                                    assert(band.artists.length === 0);
                                    assert(band.name === 'John Doe Band');
                                    done();
                                })
                        })
                })
        })
    })
})
