const assert = require('assert');
const request = require('supertest');
const app = require('../../app');
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const Artist = mongoose.model('artist');

describe('Artist routing', () => {

    it('GET /api/artists', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        artist.save()
            .then(() => {
                request(app)
                    .get('/api/artists')
                    .end(() => {
                        Artist.find()
                            .then((artists) => {
                                assert(artists.length === 1);
                                assert(artists[0].firstname === 'John');
                                done();
                            })
                    })
            })
    })

    it('GET /api/artists/:id', (done) => {
        const artist = new Artist({
            // _id: '0',
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        artist.save()
            .then(() => {
                request(app)
                    .get(`/api/artists/${artist._id}`)
                    .end(() => {
                        Artist.findOne({_id: artist._id})
                            .then((artist) => {
                                assert(artist.firstname === 'John');
                                done();
                            })
                    })
            })
    })

    it('POST /api/artists', (done) => {
        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            Artist.countDocuments().then(count => {
                request(app)
                    .post('/api/artists')
                    .set("authorization", "Bearer " + token)
                    .send({
                        firstname: 'John',
                        lastname: 'Doe',
                        nickname: 'Test Singer One',
                        birthdate: '1949-05-09T23:00:00.000Z',
                        nationality: 'netherlands',
                        stillActive: true
                    })
                    .end(() => {
                        Artist.countDocuments().then(newCount => {
                            assert(count + 1 === newCount);
                            done();
                        })
                    })
            })
        })
    })

    it('PUT /api/artists/:id', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            artist.save()
                .then(() => {
                    request(app)
                        .put(`/api/artists/${artist._id}`)
                        .set("authorization", "Bearer " + token)
                        .send({firstname: 'Piet'})
                        .end(() => {
                            Artist.findOne({firstname: 'Piet'})
                                .then(artist => {
                                    assert(artist.firstname === 'Piet');
                                    done();
                                })
                        })
                })
        })
    })

    it('DELETE /api/artists/:id', (done) => {
        const artist = new Artist({
            firstname: 'John',
            lastname: 'Doe',
            nickname: 'Test Singer One',
            birthdate: '1949-05-09T23:00:00.000Z',
            nationality: 'netherlands',
            stillActive: true
        });

        jwt.sign({id: 1}, "secret", {expiresIn: "2h"}, (err, token) => {
            artist.save()
                .then(() => {
                request(app)
                    .delete(`/api/artists/${artist._id}`)
                    .set("authorization", "Bearer " + token)
                    .end(() => {
                        Artist.findOne({_id: artist._id})
                            .then((artist) => {
                                assert(artist === null);
                                done();
                            })
                    })
                })
        })
    })
})
