const ArtistController = require('../controllers/artist_controller')
const BandController = require('../controllers/band_controller')
const SongController = require('../controllers/song_controller')
const CommentController = require('../controllers/comment_controller')
const userController = require("../controllers/user_controller");
const AuthController = require("../controllers/auth_controller");
const userRelationController = require("../controllers/userRelation_Controller");
const userSongRelationController = require("../controllers/userSongRelation_Controller");

module.exports = (app) => {

    app.use(function (req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader(
            "Access-Control-Allow-Methods",
            "GET, POST, OPTIONS, PUT, PATCH, DELETE"
        );
        res.setHeader(
            "Access-Control-Allow-Headers",
            "X-Requested-With,content-type,authorization"
        );
        res.setHeader("Access-Control-Allow-Credentials", true);
        next();
    });

    // Artist
    app.get('/api/artists', ArtistController.get);
    app.get('/api/artists/:id', ArtistController.getById);
    app.post('/api/artists', AuthController.validateToken, ArtistController.create);
    app.put('/api/artists/:id', AuthController.validateToken, ArtistController.edit);
    app.delete('/api/artists/:id', AuthController.validateToken, ArtistController.delete);

    // Band
    app.get('/api/bands', BandController.get);
    app.get('/api/bands/:id', BandController.getById);
    app.post('/api/bands', AuthController.validateToken, BandController.create);
    app.put('/api/bands/:id', AuthController.validateToken, BandController.edit);
    app.delete('/api/bands/:id', AuthController.validateToken, BandController.delete);

    // Band with Artists
    app.post('/api/bands/add/:bId/:aId', AuthController.validateToken, BandController.addArtist);
    app.delete('/api/bands/remove/:bId/:aId', AuthController.validateToken, BandController.removeArtist);

    // Song
    app.get('/api/songs', SongController.get);
    app.get('/api/songs/:id', SongController.getById);
    app.get('/api/songs/artists/:id', SongController.getSongsByArtist);
    app.get('/api/songs/bands/:id', SongController.getSongsByBand);
    app.post('/api/songs', AuthController.validateToken, SongController.create);
    app.put('/api/songs/:id', AuthController.validateToken, SongController.edit);
    app.delete('/api/songs/:id', AuthController.validateToken, SongController.delete);

    // Comment
    app.post('/api/songs/:id/comment', AuthController.validateToken, CommentController.create);
    app.put('/api/songs/:id/comment/:commentId/:userid', AuthController.validateToken, AuthController.validateRightUser, CommentController.edit);
    app.delete('/api/songs/:id/comment/:commentId/:userid', AuthController.validateToken, AuthController.validateRightUser, CommentController.delete);

    // User
    app.get('/api/users', userController.getAllUsers);
    app.get('/api/users/:id', userController.getUserById);

    // Auth
    app.post('/api/register', AuthController.register);
    app.post('/api/login', AuthController.login);

    //User Relation and follow functions
    app.put('/api/relation/:aId/:id', AuthController.validateToken, userRelationController.makeRelation);
    app.get('/api/relation/if/:id', AuthController.validateToken, userRelationController.showRelationOfPerson);
    app.get('/api/relation/af/:id', AuthController.validateToken, userRelationController.showRelationOfAnother);
    app.put('/api/relation/delete/:aId/:id', AuthController.validateToken, userRelationController.deleteRelation);
    app.get('/api/relation/checkstatus/:aId/:id', AuthController.validateToken, userRelationController.checkForFollowStatus);

    //User relation with song
    app.put('/api/liked/:uId/:sId', AuthController.validateToken, userSongRelationController.makeRelation);
    app.get('/api/liked/:id', AuthController.validateToken, userSongRelationController.showLikesOfPerson);
    app.put('/api/liked/delete/:uId/:sId', AuthController.validateToken, userSongRelationController.deleteRelation);
}


