const session = require("../../neo");
const Song = require("../models/song");

module.exports = {

    makeRelation(req, res, next) {
        const userId = req.params.uId;
        const songId = req.params.sId;

        console.log("test1");

        session.session()
            .run('MATCH (u:User), (s:Song)' +
                'WHERE id(u) = $uId AND s.songId = $sId ' +
                'MERGE (u)-[r:LIKED]->(s)' +
                'RETURN r',
                {
                    uId: parseInt(userId),
                    sId: songId
                }
            )
            .then(result => {
                console.log("test2");
                res.send("Liked song");
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => {
                session.session().close()
                console.log("test3");
            })
            .catch(next)
        console.log("test4")
    },

    showLikesOfPerson(req, res, next) {
        const userId = req.params.id;
        const songs = [];

        session.session()
            .run('MATCH (u:User)-[:LIKED]->(song) WHERE id(u) = $id RETURN song',
                {id: parseInt(userId)}
            )
            .then(result => {
                result.records.forEach(record => {
                    songs.push({
                        _id: record._fields[0].properties.songId
                    })
                })
                return songs
            })
            .then(songsId => {
                Song.find({
                    '_id': {$in: songsId}
                })
                    .then(songs => res.send(songs))
                    .catch(next);
            })
    },

    deleteRelation(req, res, next) {
        const userId = req.params.uId;
        const songId = req.params.sId;

        session.session()
            .run('MATCH (u:User)-[rel:LIKED]->(s:Song) ' +
                'WHERE id(u) = $uId AND s.songId = $sId ' +
                'DELETE rel ' +
                'RETURN u, s',
                {
                    uId: parseInt(userId),
                    sId: songId
                }
            )
            .then(result => {
                res.send("Liked is removed successful")
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
            .catch(next)
    }
}
