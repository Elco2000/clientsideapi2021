const Band = require('../models/band')

module.exports = {
    get(req, res, next) {
        Band.find({})
            .populate("artists", {firstname: 1, lastname: 1, nickname: 1})
            .then(bands => res.send(bands))
            .catch(next)
    },

    getById(req, res, next) {
        const id = req.params.id;

        Band.findById({_id: id})
            .populate("artists", {firstname: 1, lastname: 1, nickname: 1})
            .then(band => res.send(band))
            .catch(next)
    },

    create(req, res, next) {
        const bandProps = req.body;

        Band.create(bandProps)
            .then(band => res.send(band))
            .catch(next)
    },

    edit(req, res, next) {
        const id = req.params.id;
        const bandProps = req.body;

        Band.findByIdAndUpdate({_id: id}, bandProps)
            .then(() => Band.findById({_id: id}))
            .then(band => res.send(band))
            .catch(next)
    },

    delete(req, res, next) {
        const id = req.params.id;

        Band.findByIdAndRemove({_id: id})
            .then(band => res.status(204).send(band))
            .catch(next)
    },

    addArtist(req, res, next) {
        const bandId = req.params.bId;
        const artistId = req.params.aId;

        Band.findByIdAndUpdate({_id: bandId}, {$push: {artists: artistId}})
            .then(() => Band.findById({_id: bandId}))
            .then(band => res.send(band))
            .catch(next);
    },

    removeArtist(req, res, next) {
        const bandId = req.params.bId;
        const artistId = req.params.aId;

        Band.findByIdAndUpdate({"_id": bandId}, {$pull: {artists: artistId}})
            .then(band => res.status(204).send(band))
            .catch(next);
    }
}
