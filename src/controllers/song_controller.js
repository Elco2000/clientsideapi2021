const Song = require('../models/song');
const session = require("../../neo");

module.exports = {
    get(req, res, next) {
        Song.find({})
            .then(songs => res.send(songs))
            .catch(next);
    },

    getById(req, res, next) {
        const id = req.params.id;

        Song.findById({_id: id})
            .populate("artistOfSong", {nickname: 1})
            .populate("bandOfSong", {name: 1})
            .then(song => res.send(song))
            .catch(next);
    },

    getSongsByArtist(req, res, next) {
        const artistId = req.params.id;

        Song.find({artistOfSong: artistId})
            .then(songs => res.send(songs))
            .catch(next);
    },

    getSongsByBand(req, res, next) {
        const bandId = req.params.id;

        Song.find({bandOfSong: bandId})
            .then(songs => res.send(songs))
            .catch(next);
    },

    create(req, res, next) {
        const songProps = req.body;

        Song.create(songProps)

            .then(song => {
                res.send(song);
                return song;
            })


            .then(songTwo => {
                session.session()
                    .run('CREATE (a:Song {songId: $_id, title: $title}) RETURN a',
                        {
                            _id: songTwo._id.toString(),
                            title: songTwo.title.toString()
                        }
                    )
                    .then(() => session.session().close())
            })
            .catch(next);
    },

    edit(req, res, next) {
        const id = req.params.id;
        const songProps = req.body;

        Song.findByIdAndUpdate({_id: id}, songProps)
            .then(() => Song.findById({_id: id}))
            .then(song => res.send(song))
            .catch(next);
    },

    delete(req, res, next) {
        const id = req.params.id;

        Song.findByIdAndRemove({_id: id})
            .then(song => {
                res.status(204).send(song)
                return song;
            })
            .then(songTwo => {
                session.session()
                    .run('MATCH (a:Song {songId: $_id}) DELETE a',
                        {
                            _id: songTwo._id.toString(),
                        }
                    )
                    .then(() => session.session().close())
            })
            .catch(next);
    }
}
