const Artist = require('../models/artist')

module.exports = {
    get(req, res, next) {
        Artist.find({})
            .then(artists => res.send(artists))
            .catch(next)
    },

    getById(req, res, next) {
        const id = req.params.id;

        Artist.findById({_id: id})
            .then(artist => res.send(artist))
            .catch(next)
    },

    create(req, res, next) {
        const artistProps = req.body;

        Artist.create(artistProps)
            .then(artist => res.send(artist))
            .catch(next);
    },

    edit(req, res, next) {
        const id = req.params.id;
        const artistProps = req.body;

        Artist.findByIdAndUpdate({_id: id}, artistProps)
            .then(() => Artist.findById({_id: id}))
            .then(artist => res.send(artist))
            .catch(next);
    },

    delete(req, res, next) {
        const id = req.params.id;

        Artist.findByIdAndRemove({_id: id})
            .then(artist => res.status(204).send(artist))
            .catch(next);
    }
}
