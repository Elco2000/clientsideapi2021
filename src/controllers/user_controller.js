const session = require("../../neo");

module.exports = {
    getAllUsers(req, res, next) {
        session.session()
            .run('MATCH (n:User) RETURN n')
            .then(result => {
                const users = [];
                result.records.forEach(record => {
                    users.push({
                        id: record._fields[0].identity.low,
                        username: record._fields[0].properties.username,
                        email: record._fields[0].properties.email
                    })
                })
                res.send(users);
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    },

    getUserById(req, res, next) {
        const userId = req.params.id;
        session.session()
            .run('MATCH (a:User) WHERE id(a) = $id RETURN a', {id: parseInt(userId)})
            .then(result => {
                res.send({
                    id: result.records[0]._fields[0].identity.low,
                    username: result.records[0]._fields[0].properties.username,
                    email: result.records[0]._fields[0].properties.email
                })
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    }
}
