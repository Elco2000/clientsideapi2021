module.exports = {
    confirmUser (comment, owner) {
        if(!comment.user.equals(owner._id)) {
            throw Error('You must own this comment in order to edit it')
        }
    }
}
