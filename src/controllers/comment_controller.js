const Song = require('../models/song');
const Comment = require('../models/comment.schema');
const {confirmUser} = require("./helper_controller");

module.exports = {
    create(req, res, next) {
        const songId = req.params.id;
        const commentProps = req.body;

        Song.findByIdAndUpdate({_id: songId}, {$push: {comments: commentProps}})
            .then(() => Song.findById({_id: songId}))
            .then(song => res.send(song))
            .catch(next);
    },

    // Dont Work in front-end.
    edit(req, res, next) {
        const songId = req.params.id;
        const commentId = req.params.commentId;
        const commentProps = req.body;
        const userId = req.params.userid;

        console.log(commentProps);

            if(commentProps.user === userId) {
                Song.findById({"_id": songId, "comments._id": commentId})
                    .then(() => Song.findById({_id: songId}))
                    .then(song => {
                        console.log(commentProps.user);
                        const comment = song.comments.id(commentId);
                        comment.set(commentProps);
                        return song.save();
                    })
                    .then((song) => {
                        res.send(song);
                    })
                    .catch(next);
            } else {
                res.status(401).json({
                    error: "Not the same user: " + commentProps.user + " != " + userId,
                    datetime: new Date().toISOString(),
                });
            }
    },

    delete(req, res, next) {
        const songId = req.params.id;
        const commentId = req.params.commentId;
        const userId = req.params.userid;

        Song.findById({"_id": songId, "comments._id": commentId})
            .then(() => Song.findById({_id: songId}))
            .then(song => {
                const comment = song.comments.id(commentId);
                if (comment.user === userId) {
                    comment.remove();
                    return song.save();
                } else {
                    res.status(401).json({
                        error: "Not the same user",
                        datetime: new Date().toISOString(),
                    });
                }
            })
            .then(song => res.status(204).send(song))
            .catch(next);
    }
}
