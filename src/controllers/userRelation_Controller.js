const session = require("../../neo");

module.exports = {

    makeRelation(req, res, next) {
        const friendId = req.params.id;
        const userId = req.params.aId;

        session.session()
            .run('MATCH (a:User), (b:User)' +
                'WHERE id(a) = $aId AND id(b) = $bId ' +
                'MERGE (a)-[:FOLLOWS]->(b)' +
                'RETURN a, b',
                {
                    aId: parseInt(userId),
                    bId: parseInt(friendId)
                }
            )
            .then(result => {
                result.records.forEach(record => {
                    res.send("Relation created")
                })
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    },

    showRelationOfPerson(req, res, next) {
        const userId = req.params.id;
        const users = [];
        session.session()
            .run('MATCH (a:User)-[:FOLLOWS]->(friend) WHERE id(a) = $id RETURN friend',
                {id: parseInt(userId)}
            )
            .then(result => {
                result.records.forEach(record => {
                    users.push({
                        id: record._fields[0].identity.low,
                        username: record._fields[0].properties.username,
                        email: record._fields[0].properties.email
                    })
                })
                res.send(users)
                // res.send(result)
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    },

    showRelationOfAnother(req, res, next) {
        const userId = req.params.id;
        const users = [];
        session.session()
            .run('MATCH (a:User)<-[:FOLLOWS]-(friend) WHERE id(a) = $id RETURN friend',
                {id: parseInt(userId)}
            )
            .then(result => {
                result.records.forEach(record => {
                    users.push({
                        id: record._fields[0].identity.low,
                        username: record._fields[0].properties.username,
                        email: record._fields[0].properties.email
                    })
                })
                res.send(users)
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    },

    deleteRelation(req, res, next) {
        const friendId = req.params.id;
        const userId = req.params.aId;
        session.session()
            .run('MATCH (a:User)-[rel:FOLLOWS]->(b:User) ' +
                'WHERE id(a) = $aId AND id(b) = $bId ' +
                'DELETE rel ' +
                'RETURN a, b',
                {
                    aId: parseInt(userId),
                    bId: parseInt(friendId)
                }
            )
            .then(result => {
                res.send("Relation successful deleted!")
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    },

    checkForFollowStatus(req, res, next) {
        const otherPersonId = req.params.id;
        const userId = req.params.aId;

        session.session()
            .run('MATCH (a:User)-[:FOLLOWS]->(b:User) WHERE id(a) = $aId AND id(b) = $bId RETURN b',
                {
                    aId: parseInt(userId),
                    bId: parseInt(otherPersonId)
                })
            .then(result => {
                if (result.records.length === 0) {
                    res.send(false)
                } else {
                    res.send(true)
                }
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    }
}

