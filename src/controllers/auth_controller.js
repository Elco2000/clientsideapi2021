const session = require("../../neo");
const jwt = require("jsonwebtoken");

module.exports = {
    register(req, res, next) {
        const body = req.body
        session.session()
            .run('CREATE (a:User { username: $username, password: $password, email: $email}) RETURN a',
                body
            )
            .then(result => {
                result.records.forEach(record => {
                    res.send(record._fields[0].properties)
                })
            })
            .catch(error => {
                console.log(error)
            })
            .then(() => session.session().close())
    },

    login(req, res, next) {
        const body = req.body
        session.session()
            .run('MATCH (n) WHERE n.email = $email RETURN n',
                body)
            .then(result => {
                if(result.records[0] === undefined) {
                    res.status(400).json({
                        error: "User not found",
                        datetime: new Date().toISOString(),
                    });
                } else if (result.records[0]._fields[0].properties.password !== req.body.password) {
                    res.status(400).json({
                        error: "password not found",
                        datetime: new Date().toISOString(),
                    });
                } else {
                    res.send({
                        token: jwt.sign(result.records[0]._fields[0].identity.low, "secret"),
                        username: result.records[0]._fields[0].properties.username,
                        password: result.records[0]._fields[0].properties.password,
                        email: result.records[0]._fields[0].properties.email,
                    })
                }
            })
    },

    validateToken(req, res, next) {
        // console.log("validateToken called");
        // console.log(req.headers);
        const authHeader = req.headers.authorization;
        let token = "";
        if (authHeader !== undefined) {
            token = authHeader.substring(7, authHeader.length);
        }

        jwt.verify(token, "secret", (err, payload) => {
            if (err) {
                console.log("Not authorized");
                res.status(401).json({
                    error: "Not authorized",
                    datetime: new Date().toISOString(),
                });
            }
            if (payload) {
                // console.log("token is valid", payload);
                req.userId = payload.id;
                next();
            }
        });
    },

    validateRightUser(req, res, next) {
        const authHeader = req.headers.authorization;
        let token = "";
        if (authHeader !== undefined) {
            token = authHeader.substring(7, authHeader.length);
        }

        jwt.verify(token, "secret", (err, payload) => {
            if (payload !== req.params.userid) {
                res.status(401).json({
                    error: "Not authorized",
                    datetime: new Date().toISOString(),
                });
            }
            if (payload === req.params.userid) {
                // console.log("token is valid", payload);
                req.userId = payload.id;
                next();
            }
        });
    }
}
