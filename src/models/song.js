const mongoose = require('mongoose');
const CommentSchema = require("./comment.schema");
const Schema = mongoose.Schema;

const SongSchema = new Schema({
    title: {
        type: String,
        required: [true, 'A song need a title'],
        maxlength: [35, 'Title is too long max of 35 characters'],
        minlength: [2, 'Title is too short min of 2 characters']
    },
    length: {
        type: String,
        required: [true, 'A song need a length'],
        maxlength: [6, 'Length is too long max of 6 characters'],
        minlength: [4, 'Length is too short min of 4 characters']
    },
    releasedate: {
        type: Date,
        required: [true, 'A song need a releasedate'],
    },
    youtubelink: {
        type: String,
        required: [true, 'A song need a youtubelink'],
        maxlength: [80, 'Youtubelink is too long max of 35 characters'],
        minlength: [10, 'Youtubelink is too short min of 2 characters']
    },
    spotifylink: {
        type: String,
        required: [true, 'A song need a spotifylink'],
        maxlength: [80, 'Spotifylink is too long max of 35 characters'],
        minlength: [10, 'Spotifylink is too short min of 2 characters']
    },
    artistOfSong: {
        type: mongoose.Types.ObjectId,
        ref: 'artist',
        required: false,
    },
    bandOfSong: {
        type: mongoose.Types.ObjectId,
        ref: 'band',
        required: false,
    },
    comments: {
        type: [CommentSchema],
        default: [],
        required: [true, 'A song need a comments']
    }
});

const Song = mongoose.model('song', SongSchema);

module.exports = Song;
