const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
    firstname: {
        type: String,
        required: [true, 'A artist need a firstname'],
        maxlength: [20, 'Firstname is too long max of 20 characters'],
        minlength: [2, 'Firstname is too short min of 2 characters']
    },
    lastname: {
        type: String,
        required: [true, 'A artist need a lastname'],
        maxlength: [35, 'Lastname is too long max of 35 characters'],
        minlength: [2, 'Lastname is too short min of 2 characters']
    },
    nickname: {
        type: String,
        required: [true, 'A artist need a nickname'],
        maxlength: [20, 'Nickname is too long max of 20 characters'],
        minlength: [2, 'Nickname is too short min of 2 characters']
    },
    birthdate: {
        type: Date,
        required: [true, 'A artist need a birthdate']
    },
    nationality: {
        type: String,
        required: [true, 'A artist need a nationality'],
    },
    stillActive: {
        type: Boolean,
        required: [true, 'A artist need a stillActive attribute'],
    },
});

const Artist = mongoose.model('artist', ArtistSchema);

module.exports = Artist;

