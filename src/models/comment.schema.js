const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    title: {
        type: String,
        required: [true, 'A comment need a title'],
        maxlength: [40, 'Title is too long max of 40 characters'],
        minlength: [2, 'Title is too short min of 2 characters']
    },
    text: {
        type: String,
        required: [true, 'A comment need a text'],
        maxlength: [200, 'Text is too long max of 200 characters'],
        minlength: [6, 'Text is too short min of 6 characters']
    },
    dateOfCreate: {
        type: Date,
        required: [true, 'A comment need a dateOfCreate']
    },
    user: {
        type: String,
        required: [true, 'A comment need a user'],
        maxlength: [4, 'Title is too long max of 4 characters'],
    }
})

module.exports = CommentSchema;
