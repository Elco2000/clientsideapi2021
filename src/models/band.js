const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BandSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A band need a name'],
        maxlength: [35, 'Name is too long max of 35 characters'],
        minlength: [2, 'Name is too short min of 2 characters']
    },
    stillActive: {
        type: Boolean,
        required: [true, 'A band need a stillActive']
    },
    artists: {
        type: [mongoose.Types.ObjectId],
        ref: 'artist',
        required: [true, 'A band need a artists']
    }
});

const Band = mongoose.model('band', BandSchema);

module.exports = Band;
